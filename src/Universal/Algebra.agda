module Universal.Algebra where

open import Algebra
open import Data.Nat using (ℕ)
open import Level
open import Function.Nary.NonDependent
open import Data.Unit.Polymorphic
open import Data.Product
open import Agda.Primitive

Language : (ℓ : Level) → Set _
Language ℓ = ℕ → Set ℓ

replicateLevel : ∀ n (ℓ : Level) → Levels n 
replicateLevel ℕ.zero ℓ = tt
replicateLevel (ℕ.suc n) ℓ = ℓ , (replicateLevel n ℓ)

replicateSet : ∀ {ℓ} n → (A : Set ℓ) → Sets n (replicateLevel n ℓ)
replicateSet ℕ.zero A = tt
replicateSet (ℕ.suc n) A = A , replicateSet n A

Opₙ : ∀ {ℓ} n → Set ℓ → Set (ℓ ⊔ ⨆ n (replicateLevel n ℓ))
Opₙ n A = replicateSet n A ⇉ A

Interpretation : ∀ {ℓ Lℓ} → Language Lℓ → Set ℓ → Setω
Interpretation L A = ∀ {n} (f : L n) → Opₙ n A
